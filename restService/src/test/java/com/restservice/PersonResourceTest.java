package com.restservice;

import com.restservice.client.PersonClientApi;
import com.restservice.client.PersonClientApiImpl;
import com.restservice.model.address.Address;
import com.restservice.model.person.Person;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.servlet.ServletException;
import java.io.File;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Ignore
public class PersonResourceTest {
    private static  EmbeddedServer embeddedServer;
    private static PersonClientApi personClientApi;

    private static final int startCount = 10;

    @BeforeClass
    public static void startServer() throws ServletException {
        embeddedServer = new EmbeddedServer(8090, "/restService");
        embeddedServer.start();
        personClientApi = new PersonClientApiImpl("8090");

        for (int i = 1; i <= startCount; i++){
            personClientApi.addPerson(new Person("name" + i, "myPos" + i, 10 + i));
        }

    }

    @AfterClass
    public static void stopServer() {
        embeddedServer.stop();
    }

    @Test
    public void getPersons() throws Exception {
        List<Person> persons = personClientApi.getPersons();

        assertNotEquals("getPersons return wrong count of employees", 0, persons.size());
    }

    @Test
    public void getPersonsPage() throws Exception {
        List<Person> persons = personClientApi.getPersons(1, 10);

        assertNotEquals("getPersons return wrong count of employees", 0, persons.size());
    }

    @Test
    public void testAddAndRemovePerson() throws Exception {
        int startSize = personClientApi.getPersons().size();
        int curSize;
        Person person = personClientApi.addPerson(new Person("name1", "myPos1", 100));

        curSize = personClientApi.getPersons().size();

        assertEquals("add person does not work", startSize + 1, curSize);
        assertNotEquals("person id is not generated", 0, person.getId());

        personClientApi.deletePerson(person.getId());

        curSize = personClientApi.getPersons().size();

        assertEquals("delete person does not work", startSize, curSize);
    }

    @Test
    public void countOfPages() throws Exception {
        int startSize = personClientApi.getPersons().size();

        int actualPageCount = personClientApi.getPageCount(5);
        int expectedPageCount = (startSize - 1) / 5 + 1;
        assertEquals("wrong page count", expectedPageCount, actualPageCount);

        actualPageCount = personClientApi.getPageCount(7);
        expectedPageCount = (startSize - 1) / 7 + 1;
        assertEquals("wrong page count", expectedPageCount, actualPageCount);
    }

    @Test
    public void getPersonById() throws Exception {
        Person expectedPerson = personClientApi.getPersons().get(0);

        Person actualPerson = personClientApi.getPerson(expectedPerson.getId());

        assertEquals("getPersonById return wrong object", expectedPerson, actualPerson);
    }

    @Test
    public void updateEmployee() throws Exception {
        Person person = personClientApi.addPerson(new Person("Vasya", "manager", 153));

        person.setName("Kolya");
        person.addAddress(new Address("state", "Washington", "22234", "high street, 17"));
        person.setPosition("director");

        personClientApi.editPerson(person);
        Person newPerson = personClientApi.getPerson(person.getId());

        assertEquals("person is not updated", person, newPerson);
    }

    @Test
    public void addAndDeleteAddress() throws Exception {
        Person person = personClientApi.addPerson(new Person("Vasya", "manager", 153));

        personClientApi.addAddress(person.getId(), new Address("state", "Washington", "22237", "high street, 17"));
        person = personClientApi.getPerson(person.getId());

        assertEquals("address is not added to person", 1, person.getAddresses().size());

        Iterator<Address> addressIterator = person.getAddresses().iterator();
        Address address = addressIterator.next();
        personClientApi.removeAddress(person.getId(), address.getId());
        person = personClientApi.getPerson(person.getId());

        assertEquals("address is not added to person", 0, person.getAddresses().size());
    }
}

class EmbeddedServer implements Runnable {

    private Tomcat tomcat;
    private Thread serverThread;

    public EmbeddedServer(int port, String contextPath) throws ServletException {
        tomcat = new Tomcat();
        tomcat.setPort(port);
        tomcat.setBaseDir("target/tomcat");
        tomcat.addWebapp(contextPath, new File("src/main/webapp").getAbsolutePath());
        serverThread = new Thread(this);
    }

    public void start() {
        serverThread.start();
    }

    public void run() {
        try {
            tomcat.start();
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
        tomcat.getServer().await();
    }

    public void stop() {
        try {
            tomcat.stop();
            tomcat.destroy();
            deleteDirectory(new File("target/tomcat/"));
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
    }

    void deleteDirectory(File path) {
        if (path == null) return;
        if (path.exists()) {
            for (File f : path.listFiles()) {
                if (f.isDirectory()) {
                    deleteDirectory(f);
                    f.delete();
                } else {
                    f.delete();
                }
            }
            path.delete();
        }
    }
}
