package com.restservice.before;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.ServletException;

public class TestTwo{
    @BeforeClass
    public static void init() throws ServletException {
        System.out.println("before class 2");
    }

    @Test
    public void test() throws Exception {
        System.out.println("test 2");
    }

    @AfterClass
    public static void destroy() throws ServletException {
        System.out.println("after class 2");
    }
}
