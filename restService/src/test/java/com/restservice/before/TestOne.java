package com.restservice.before;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.ServletException;

public class TestOne{
    @BeforeClass
    public static void init() throws ServletException {
        System.out.println("before class 1");
    }

    @Test
    public void test() throws Exception {
        System.out.println("test 1");
    }

    @AfterClass
    public static void destroy() throws ServletException {
        System.out.println("after class 1");
    }
}
