package com.restservice.before;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import javax.servlet.ServletException;

@RunWith(Suite.class)
@Suite.SuiteClasses( { TestOne.class, TestTwo.class })
public class BaseClass {
    @BeforeClass
    public static void initAll() throws ServletException {
        System.out.println("before all classes");
    }

    @AfterClass
    public static void destroyAll() throws ServletException {
        System.out.println("after all classes");
    }
}
