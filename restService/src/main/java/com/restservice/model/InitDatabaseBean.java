package com.restservice.model;

import com.restservice.model.address.Address;
import com.restservice.model.dao.AddressDao;
import com.restservice.model.dao.PersonDao;
import com.restservice.model.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@Component
public class InitDatabaseBean {
    @Autowired
    private PersonDao personDao;
    @Autowired
    private AddressDao addressDao;

    private Random random = new Random();

    @PostConstruct
    public void init(){
        if (!addressDao.getAddresses().isEmpty()){
            return;
        }

        for (int i = 0; i < 100; i++) {
            addressDao.addAddress(new Address("Alabama", "Birmingham", "21351", "address " + i));
        }

        for (int i = 0; i < 100; i++) {
            Person person = new Person("Vasya", "Cleaner", random.nextInt(1000));
            int addressCount = random.nextInt(3) + 1;
            Set<Address> addresses = new HashSet<Address>();
            for (int j = 0; j < addressCount; j++) {
                Address address = addressDao.getAddress(random.nextInt(100) + 1);
                addresses.add(address);
            }

            person.setAddresses(addresses);
            personDao.addPerson(person);
        }
    }
}
