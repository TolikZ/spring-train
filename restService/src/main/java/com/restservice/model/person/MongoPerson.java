package com.restservice.model.person;

import com.restservice.model.address.Address;
import org.bson.types.ObjectId;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement
@org.mongodb.morphia.annotations.Entity
@NamedQuery(name = "Person.pageCount", query = "SELECT COUNT(p) FROM Person p")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MongoPerson {
    @org.mongodb.morphia.annotations.Id
    private ObjectId id;
    private String name;
    private String position;
    private int salary;
    @Embedded
    private Set<Address> addresses;

    public MongoPerson() {
    }

    public MongoPerson(String name, String position, int salary) {
        this.name = name;
        this.position = position;
        this.salary = salary;
        addresses = new HashSet<Address>();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public void addAddress(Address address){
        addresses.add(address);
    }

    public void removeAddress(Address address){
        addresses.remove(address);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MongoPerson person = (MongoPerson) o;

        if (id != person.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + salary;
        result = 31 * result + (addresses != null ? addresses.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                ", addresses=" + Arrays.toString(addresses.toArray()) +
                '}';
    }
}
