package com.restservice.model.dao;

import com.restservice.model.person.Person;

import java.util.List;

public interface PersonDao {
    public List<Person> getPersons();
    public List<Person> getPersons(int page, int personPerPage);
    public int getPageCount(int personPerPage);
    public Person getPerson(int id);
    public Person addPerson(Person person);
    public boolean deleteEmployeeById(int id);
    public void updatePerson(Person person);
}
