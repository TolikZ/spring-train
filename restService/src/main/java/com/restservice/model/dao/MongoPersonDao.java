package com.restservice.model.dao;

import com.mongodb.MongoClient;
import com.restservice.model.address.Address;
import com.restservice.model.person.MongoPerson;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;

import java.net.UnknownHostException;
import java.util.List;

public class MongoPersonDao{
    private Datastore ds;

    public MongoPersonDao(){
        Morphia morphia = new Morphia();
        MongoClient mongoClient = null;
        try {
            mongoClient = new MongoClient("localhost", 27017);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            throw new Error("Can not create database connection");
        }
        morphia.map(MongoPerson.class).map(Address.class);
        ds = morphia.createDatastore(mongoClient, "persons");
    }

    public List<MongoPerson> getPersons() {
        return ds.find(MongoPerson.class).asList();
    }

    public List<MongoPerson> getPersons(int page, int personPerPage) {
        return ds.find(MongoPerson.class).limit(personPerPage).offset((page-1) * personPerPage).asList();
    }

    public int getPageCount(int personPerPage) {
        int personCount = (int) ds.getCount(MongoPerson.class);
        return (personCount - 1) / personPerPage + 1;
    }

    public MongoPerson getPerson(String id) {
        return ds.get(MongoPerson.class, id);
    }

    public MongoPerson addPerson(MongoPerson person) {
        Key key = ds.save(person);
        return ds.getByKey(MongoPerson.class, key);
    }

    public boolean deleteEmployeeById(String id) {
        MongoPerson person = getPerson(id);
        ds.delete(person);
        return true;
    }

    public void updatePerson(MongoPerson person) {
        ds.save(person);
    }
}
