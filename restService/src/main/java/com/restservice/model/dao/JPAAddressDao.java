package com.restservice.model.dao;

import com.restservice.model.address.Address;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRED)
public class JPAAddressDao implements AddressDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Address> getAddresses() {
        return entityManager.createQuery("from Address")
                .getResultList();
    }

    @Override
    public Address getAddress(int id) {
        return entityManager.find(Address.class, id);
    }

    @Override
    public void addAddress(Address address) {
        if (address.getId() != 0) {
            Address curAddress = getAddress(address.getId());
            if (curAddress != null){
                entityManager.merge(address);
            } else{
                address.setId(0);
            }
        }

        if (address.getId() == 0){
            entityManager.persist(address);
        }

    }

    @Override
    public boolean deleteAddressById(int id) {
        Address address = getAddress(id);
        if (address != null){
            entityManager.remove(address);
            return true;
        }
        return false;
    }

    @Override
    public void updateAddress(Address address) {
        entityManager.merge(address);
    }

    public Address findAddressByAddressLine(String addressLine){
         List<Address> addresses = entityManager.createNamedQuery("Address.findAddressByAddressLine")
                .setParameter("addressLine", addressLine)
                .getResultList();
         if (addresses.isEmpty()){
             return null;
         } else{
             return addresses.get(0);
         }
    }
}
