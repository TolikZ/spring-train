package com.restservice.model.dao;

import com.restservice.model.address.Address;
import com.restservice.model.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(propagation = Propagation.REQUIRED)
public class JPAPersonDao implements PersonDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private AddressDao addressDao;

    @Override
    public List<Person> getPersons() {
        return entityManager.createQuery("from Person")
                .getResultList();
    }

    @Override
    public List<Person> getPersons(int page, int personPerPage) {
        Query query =  entityManager.createQuery("from Person");
        query.setFirstResult((page-1) * personPerPage);
        query.setMaxResults(personPerPage);
        return query.getResultList();
    }

    @Override
    public int getPageCount(int personPerPage) {
//        Query query =  entityManager.createQuery("SELECT COUNT(p) FROM Person p");
        Query query =  entityManager.createNamedQuery("Person.pageCount");
        Number countNumber = (Number)query.getSingleResult();
        return (countNumber.intValue() - 1) / personPerPage + 1;
    }

    @Override
    public Person getPerson(int id) {
        return entityManager.find(Person.class, id);
    }

    @Override
    public Person addPerson(Person person) {
        for (Address address : person.getAddresses()) {
            addressDao.addAddress(address);
        }
        entityManager.persist(person);
        return person;
    }

    @Override
    public boolean deleteEmployeeById(int id) {
        Person person = getPerson(id);
        if (person != null){
            entityManager.remove(person);
            return true;
        }
        return false;
    }

    @Override
    public void updatePerson(Person person) {
        for (Address address : person.getAddresses()) {
            addressDao.addAddress(address);
        }
        entityManager.merge(person);
    }
}
