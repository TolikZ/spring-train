package com.restservice.model.dao;

import com.restservice.model.person.Person;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class HashMapPersonDao implements PersonDao {
    private Map<Integer, Person> persons = new ConcurrentHashMap<Integer, Person>();
    @Override
    public List<Person> getPersons() {
        return new ArrayList(persons.values());
    }

    @Override
    public List<Person> getPersons(int page, int personPerPage) {
        return null;
    }

    @Override
    public int getPageCount(int personPerPage) {
        return 0;
    }

    @Override
    public Person getPerson(int id) {
        return persons.get(id);
    }

    @Override
    public Person addPerson(Person person) {
        persons.put(person.getId(), person);
        return persons.get(person.getId());
    }

    @Override
    public boolean deleteEmployeeById(int id){
        if (persons.remove(id) != null){
            return true;
        }
        return false;
    }

    @Override
    public void updatePerson(Person person) {
        persons.put(person.getId(), person);
    }

    @PostConstruct
    public void init(){
//        persons.put(1, new Person(1, "Vasya", "Cleaner", 500));
//        persons.put(2, new Person(2, "Valerii Georgievich", "Director", 150));
//        persons.put(3, new Person(3, "Petrovich", "Driver", 300));
    }
}
