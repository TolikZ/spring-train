package com.restservice.model.dao;

import com.restservice.model.address.Address;

import java.util.List;

public interface AddressDao {
    public List<Address> getAddresses();
    public Address getAddress(int id);
    public void addAddress(Address address);
    public boolean deleteAddressById(int id);
    public void updateAddress(Address address);
    public Address findAddressByAddressLine(String addressLine);
}
