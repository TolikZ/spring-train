package com.restservice.client;

import com.restservice.model.address.Address;
import com.restservice.model.person.Person;

import java.util.List;

public interface PersonClientApi {
    public List<Person> getPersons();
    public List<Person> getPersons(int page, int personPerPage);
    public int getPageCount(int personPerPage);
    public Person getPerson(int id);
    public Person editPerson(Person person);
    public void deletePerson(int id);
    public Person addPerson(Person person);
    public Person addAddress(int personId, Address address);
    public Person removeAddress(int personId, int addressId);
}
