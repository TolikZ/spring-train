package com.restservice.client;


import com.restservice.model.address.Address;
import com.restservice.model.person.Person;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class ClientTest {
    public static void main(String[] args){
//        PersonClientApi personClientApi = PersonClientApiImpl.getInstance();
        PersonClientApi personClientApi = new PersonClientApiImpl();




//        service.path("restService").path("employee")
//                .accept(MediaType.APPLICATION_JSON).put(new Person(5, "Kolya", "pos", 5));



//        service.path("restService").path("employee").path(Integer.toString(2)).delete();


//        List<Person> persons = personClientApi.getPersons();
//
//        for (Person person : persons) {
//            System.out.println(person.getId());
//        }

        Person person = personClientApi.getPerson(1);
        System.out.println(person);
        person.setPosition("Cleaner");
        personClientApi.editPerson(person);
//        personClientApi.deletePerson(2);
        person = personClientApi.getPerson(1);
        System.out.println(person);

        person = personClientApi.addPerson(new Person("name1", "pos1", 100));
        System.out.println(person);

        Address address1 = new Address("str1", "str2", "str3", "address 50");
        Address address2 = new Address("str1", "str2", "str3", "address 102");
        person = personClientApi.addAddress(person.getId(), address1);
        person = personClientApi.addAddress(person.getId(), address2);
        System.out.println(person);

        person = personClientApi.removeAddress(person.getId(), 51);
        System.out.println(person);



    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080").build();
    }
}
