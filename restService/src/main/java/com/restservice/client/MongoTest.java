package com.restservice.client;

import com.restservice.model.address.Address;
import com.restservice.model.dao.MongoPersonDao;
import com.restservice.model.person.MongoPerson;

public class MongoTest {
    public static void main(String[] args) {
        MongoPersonDao dao = new MongoPersonDao();

        MongoPerson person = new MongoPerson("myname", "mypos", 122);
        Address address = new Address("mystate", "mycity", "220302", "street, 17");
        Address address2 = new Address("mystate2", "mycity2", "2203022", "street, 18");
        person.addAddress(address);
        person.addAddress(address2);
        person = dao.addPerson(person);
        System.out.println(person);
        System.out.println(dao.getPersons().size());
        System.out.println(dao.getPersons(2, 3));
    }
}
