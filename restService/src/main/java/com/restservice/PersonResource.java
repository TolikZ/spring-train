
package com.restservice;

import com.restservice.model.address.Address;
import com.restservice.model.dao.AddressDao;
import com.restservice.model.dao.PersonDao;
import com.restservice.model.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("employee")
public class PersonResource {
    @Autowired
    private PersonDao personDao;

    @Autowired
    private AddressDao addressDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getAllEmployees() {
        if (personDao == null) {
            throw new RuntimeException("personDao didn't init");
        }

        return personDao.getPersons();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("page/{page}/personPerPage/{personPerPage}")
    public List<Person> getEmployees(@PathParam("page") int page, @PathParam("personPerPage") int personPerPage) {
        if (personDao == null) {
            throw new RuntimeException("personDao didn't init");
        }

        return personDao.getPersons(page, personPerPage);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("countOfPages/personPerPage/{personPerPage}")
    public int getEmployees(@PathParam("personPerPage") int personPerPage) {
        if (personDao == null) {
            throw new RuntimeException("personDao didn't init");
        }

        return personDao.getPageCount(personPerPage);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getEmployeeById(@PathParam("id") int id) {
        if (personDao == null) {
            throw new RuntimeException("personDao didn't init");
        }
        Person result =  personDao.getPerson(id);
        if (result == null){
            throw new RuntimeException("personDao didn't found");
        }
        return result;
    }

    @DELETE
    @Path("{id}")
    public void deleteEmployee(@PathParam("id") int id) {
        if (personDao.deleteEmployeeById(id) != true) {
            throw new RuntimeException("can't delete person with id = " + id);
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person addEmployee(Person person) {
        personDao.addPerson(person);
        return person;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Person updateEmployee(Person person) {
        personDao.updatePerson(person);
        return personDao.getPerson(person.getId());
    }

    @DELETE
    @Path("{personId}/address/{addressId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person deleteAddressFromEmployee(@PathParam("personId") int personId, @PathParam("addressId") int addressId) {
        Person person = personDao.getPerson(personId);
        Address address = addressDao.getAddress(addressId);
        if(person == null){
            throw new RuntimeException("person not found");
        }
        if (address == null){
            throw new RuntimeException("address not found");
        }

        person.removeAddress(address);
        personDao.updatePerson(person);

        return person;
    }

    @POST
    @Path("{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person addAddress(@PathParam("personId") int personId, Address address) {
        Person person = personDao.getPerson(personId);

        Address resAddress = addressDao.findAddressByAddressLine(address.getAddressLine());
        if (resAddress == null){
            addressDao.addAddress(address);
            resAddress = address;
        }
        person.addAddress(resAddress);

        personDao.updatePerson(person);
        return personDao.getPerson(person.getId());
    }
}
