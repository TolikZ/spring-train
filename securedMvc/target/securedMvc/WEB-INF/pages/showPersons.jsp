<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
	<c:forEach var="person" items="${persons}">
        ${person.id}, ${person.name}, ${person.position}, ${person.salary}<br/>
	</c:forEach>
</body>
</html>