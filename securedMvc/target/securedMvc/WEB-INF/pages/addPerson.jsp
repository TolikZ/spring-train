<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
	<form:form commandName="newPerson"><br />
    id <form:input path="id"/><br />
     name <form:input path="name"/><br />
      position <form:input path="position"/><br />
      salary <form:input path="salary"/><br />
      <input type="submit" value="Add person"/><br />
    </form:form>
    <h4>count of errors: ${validationErrorCount}</h4>

    <h5>${userName}/${userPassword}</h5>
</body>
</html>