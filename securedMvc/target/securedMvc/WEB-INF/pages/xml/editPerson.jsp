<%@ page contentType="text/xml" %><?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type='text/xsl' href='${pageContext.request.contextPath}/resources/xsl/editPerson.xsl'?>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<data>
    <person>
        <id>${editedPerson.id}</id>
        <name>${editedPerson.name}</name>
        <position>${editedPerson.position}</position>
        <salary>${editedPerson.salary}</salary>
        <addresses>
            <c:forEach var="address" items="${editedPerson.addresses}">
                <address>
                    <id>${address.id}</id>
                    <state>${address.state}</state>
                    <city>${address.city}</city>
                    <zipCode>${address.zipCode}</zipCode>
                    <addressLine>${address.addressLine}</addressLine>
                </address>
            </c:forEach>
        </addresses>
    </person>
</data>