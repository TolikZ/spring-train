<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
	<form:form commandName="editedPerson" action="/myWebApp/addPerson"><br />
        id: ${editedPerson.id}<br />
        <form:hidden path="id"/>
        name <form:input path="name"/><br />
        position <form:input path="position"/><br />
        salary <form:input path="salary"/><br />
        <input type="submit" value="Add person"/><br />
    </form:form>
</body>
</html>