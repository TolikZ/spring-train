<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
            </head>
            <body>
                <xsl:apply-templates select="data/person"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="person">
        <form id="editedPerson" action="/myWebApp/xml/editPerson" method="post"><br />
            id: <xsl:value-of select="id"/><br />
            <input id="id" name="id" type="hidden">
                <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
            </input>
            name <input id="name" name="name" type="text">
                <xsl:attribute name="value"><xsl:value-of select="name"/></xsl:attribute>
            </input><br/>
            position <input id="position" name="position" type="text">
                <xsl:attribute name="value"><xsl:value-of select="position"/></xsl:attribute>
            </input><br/>
            salary <input id="salary" name="salary" type="text">
                <xsl:attribute name="value"><xsl:value-of select="salary"/></xsl:attribute>
            </input><br/>

            <h3>addresses:</h3>
            <table class="table table-bordered">
                <thead>
                    <th>address id</th>
                    <th>address state</th>
                    <th>address city</th>
                    <th>address zip code</th>
                    <th>address line</th>
                    <th></th>
                </thead>
                <tbody>
                    <xsl:apply-templates select="addresses/address"/>
                </tbody>
            </table>
            <input type="submit" value="Update person"/><br/>
        </form>
        <br/><br/>
        <h3>add address:</h3>
        <form id="addedAddress" action="/myWebApp/xml/edit/2/addAddress" method="post">
            <input id="id" name="id" value="0" type="hidden"/>
            address state: <input id="state" name="state" type="text" value=""/><br/>
            address city: <input id="city" name="city" type="text" value=""/><br/>
            address zip code: <input id="zipCode" name="zipCode" type="text" value=""/><br/>
            address line: <input id="addressLine" name="addressLine" type="text" value=""/><br/>
            <input type="submit" value="Add address"/>
        </form>
    </xsl:template>

    <xsl:template match="person/addresses/address">
        <tr>
            <input type="hidden">
                <xsl:attribute name="value"><xsl:value-of select="id"/></xsl:attribute>
                <xsl:attribute name="id">addresses<xsl:value-of select="position()-1"/>.id</xsl:attribute>
                <xsl:attribute name="name">addresses[<xsl:value-of select="position()-1"/>].id</xsl:attribute>
            </input>
            <td><xsl:value-of select="id"/></td>
            <td><input type="text">
                <xsl:attribute name="value"><xsl:value-of select="state"/></xsl:attribute>
                <xsl:attribute name="id">addresses<xsl:value-of select="position()-1"/>.state</xsl:attribute>
                <xsl:attribute name="name">addresses[<xsl:value-of select="position()-1"/>].state</xsl:attribute>
            </input></td>
            <td><input type="text">
                <xsl:attribute name="value"><xsl:value-of select="city"/></xsl:attribute>
                <xsl:attribute name="id">addresses<xsl:value-of select="position()-1"/>.city</xsl:attribute>
                <xsl:attribute name="name">addresses[<xsl:value-of select="position()-1"/>].city</xsl:attribute>
            </input></td>
            <td><input type="text">
                <xsl:attribute name="value"><xsl:value-of select="zipCode"/></xsl:attribute>
                <xsl:attribute name="id">addresses<xsl:value-of select="position()-1"/>.zipCode</xsl:attribute>
                <xsl:attribute name="name">addresses[<xsl:value-of select="position()-1"/>].zipCode</xsl:attribute>
            </input></td>
            <td><input type="text">
                <xsl:attribute name="value"><xsl:value-of select="addressLine"/></xsl:attribute>
                <xsl:attribute name="id">addresses<xsl:value-of select="position()-1"/>.addressLine</xsl:attribute>
                <xsl:attribute name="name">addresses[<xsl:value-of select="position()-1"/>].addressLine</xsl:attribute>
            </input></td>
            <td>
                <button type="button">
                    <xsl:attribute name="onclick">
                        location.href = '/myWebApp/xml/edit/<xsl:value-of select="../../id"/>/removeAddress/<xsl:value-of select="id"/>';
                    </xsl:attribute>
                    remove address
                </button>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>