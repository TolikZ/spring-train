<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
            </head>
            <body>
                <xsl:call-template name="person"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template name="person">
        <form id="newPerson" action="/myWebApp/xml/add" method="post"><br />
            id <br />
            <input id="id" name="id" value="0" type="hidden"/>
            name <input id="name" name="name" type="text" value=""/><br/>
            position <input id="position" name="position" type="text" value=""/><br/>
            salary <input id="salary" name="salary" type="text" value="0"/><br/>
            <xsl:call-template name="addresses"/>
            <input type="submit" value="Add person"/><br/>
        </form>
    </xsl:template>

    <xsl:template name="addresses">
        <h3>addresses:</h3>
        <table>
            <thead>
                <th>address state</th>
                <th>address city</th>
                <th>address zip code</th>
                <th>address line</th>
                <th></th>
            </thead>
            <tbody>
                <tr>
                    <input id="addresses0.id" name="addresses[0].id" type="hidden" value="0"/>
                    <td><input id="addresses0.state" name="addresses[0].state" type="text" value=""/></td>
                    <td><input id="addresses0.city" name="addresses[0].city" type="text" value=""/></td>
                    <td><input id="addresses0.zipCode" name="addresses[0].zipCode" type="text" value=""/></td>
                    <td><input id="addresses0.addressLine" name="addresses[0].addressLine" type="text" value=""/></td>
                </tr>
            </tbody>
        </table>
    </xsl:template>

</xsl:stylesheet>