<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
            </head>
            <body>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>person id</th>
                            <th>person name</th>
                            <th>person  position</th>
                            <th>person salary</th>
                            <th>addresses</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:apply-templates select="data/persons/person"/>
                    </tbody>
                </table>
                pages:
                <xsl:call-template name="pages">
                    <xsl:with-param name="page" select="number(1)"/>
                </xsl:call-template>

            </body>
        </html>
    </xsl:template>
    <xsl:template match="person">
        <tr>
            <td>
                <a>
                    <xsl:attribute name="href">/myWebApp/xml/<xsl:value-of select="id"/></xsl:attribute>
                    <xsl:value-of select="id"/>
                </a>
            </td>
            <td><xsl:value-of select="name"/></td>
            <td><xsl:value-of select="position"/></td>
            <td><xsl:value-of select="salary"/></td>
            <td>
                <xsl:apply-templates select="addresses/address"/>
            </td>
            <td>
                <form>
                    <xsl:attribute name="action">/myWebApp/xml/remove/<xsl:value-of select="id"/></xsl:attribute>
                    <input type="submit" value="Remove"/>
                </form>
            </td>
            <td>
                <form>
                    <xsl:attribute name="action">/myWebApp/xml/edit/<xsl:value-of select="id"/></xsl:attribute>
                    <input type="submit" value="Edit"/>
                </form>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="person/addresses/address">
        <xsl:value-of select="id"/>,
        <xsl:value-of select="state"/> <xsl:value-of select="city"/> <xsl:value-of select="zipCode"/> <xsl:value-of select="addressLine"/><br/>
    </xsl:template>

    <xsl:template name="pages">
        <xsl:param name="page" select="nubmer(1)"/>
        <xsl:param name="pageCount" select="/data/pageCount"/>
        <xsl:if test="$page &lt;= $pageCount">
            <a>
                <xsl:attribute name="href">/myWebApp/xml/page/<xsl:value-of select="$page"/></xsl:attribute>
                <xsl:value-of select="$page"/>
            </a>
            <xsl:call-template name="pages">
                <xsl:with-param name="page" select="$page + 1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>