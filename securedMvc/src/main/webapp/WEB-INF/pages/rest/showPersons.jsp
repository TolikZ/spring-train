<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>
<body>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>person id</th>
                <th>person name</th>
                <th>person  position</th>
                <th>person salary</th>
                <th>addresses</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
	        <c:forEach var="person" items="${persons}">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/rest/${person.id}">${person.id}</a>
                </td>
                <td>${person.name}</td>
                <td>${person.position}</td>
                <td>${person.salary}</td>
                <td>

                        <c:forEach var="address" items="${person.addresses}">
                                ${address.id},
                                ${address.state}
                                ${address.city}
                                ${address.zipCode}
                                ${address.addressLine}
                            <br/>
                        </c:forEach>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/rest/remove/${person.id}">
                        <input type="submit" value="Remove">
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/rest/edit/${person.id}">
                        <input type="submit" value="Edit">
                    </form>
                </td>
            </tr>
	        </c:forEach>
	    </tbody>
	</table>
	pages:
	<c:forEach var="i" begin="1" end="${pageCount}">
	    <a href="${pageContext.request.contextPath}/rest/page/${i}">${i}</a>
	</c:forEach>
</body>
</html>