<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<form:form commandName="editedPerson" method="post" action="${pageContext.request.contextPath}/rest/editPerson"><br />
        id: ${editedPerson.id}<br />
        <form:hidden path="id"/>
        name <form:input path="name"/><br/>
        position <form:input path="position"/><br/>
        salary <form:input path="salary"/><br/>

        <h3>addresses:</h3>
        <table class="table table-bordered">
            <thead>
                <th>address id</th>
                <th>address state</th>
                <th>address city</th>
                <th>address zip code</th>
                <th>address line</th>
                <th></th>
            </thead>
            <tbody>
            <c:forEach var="address" items="${editedPerson.addresses}" varStatus="status">
                <tr>
                    <form:hidden path="addresses[${status.index}].id"/>
                    <td>${address.id}</td>
                    <td><form:input path="addresses[${status.index}].state" value="${address.state}"/></td>
                    <td><form:input path="addresses[${status.index}].city" value="${address.city}"/></td>
                    <td><form:input path="addresses[${status.index}].zipCode" value="${address.zipCode}"/></td>
                    <td><form:input path="addresses[${status.index}].addressLine" value="${address.addressLine}"/></td>
                    <td>
                        <button type="button" onclick="location.href = '${pageContext.request.contextPath}/rest/edit/${editedPerson.id}/removeAddress/${address.id}';">
                            remove address
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="submit" value="Update person"/><br/>
    </form:form>
    <br/><br/>
    <h3>add address:</h3>
    <form:form commandName="addedAddress" method="post" action="${pageContext.request.contextPath}/rest/edit/${editedPerson.id}/addAddress">
        <form:hidden path="id" value="0"/>
        address state: <form:input path="state" value="${address.state}"/><br/>
        address city: <form:input path="city" value="${address.city}"/><br/>
        address zip code: <form:input path="zipCode" value="${address.zipCode}"/><br/>
        address line: <form:input path="addressLine" value="${address.addressLine}"/><br/>
        <input type="submit" value="Add address"/>
    </form:form>

</body>
</html>