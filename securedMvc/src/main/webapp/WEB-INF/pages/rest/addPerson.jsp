<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<form:form commandName="newPerson" method="post" action="${pageContext.request.contextPath}/rest/add"><br />
        id ${editedPerson.id}<br />
        <form:hidden path="id" value="0"/>
        name <form:input path="name" value=""/><br/>
        position <form:input path="position" value=""/><br/>
        salary <form:input path="salary" value=""/><br/>

        <h3>addresses:</h3>
        <table>
            <thead>
                <th>address state</th>
                <th>address city</th>
                <th>address zip code</th>
                <th>address line</th>
                <th></th>
            </thead>
            <tbody>
            <tr>
                <form:hidden path="addresses[0].id"/>
                <td><form:input path="addresses[0].state" value="${address.state}"/></td>
                <td><form:input path="addresses[0].city" value="${address.city}"/></td>
                <td><form:input path="addresses[0].zipCode" value="${address.zipCode}"/></td>
                <td><form:input path="addresses[0].addressLine" value="${address.addressLine}"/></td>
            </tr>
            </tbody>
        </table>
        <input type="submit" value="Add person"/><br/>
    </form:form>
</body>
</html>