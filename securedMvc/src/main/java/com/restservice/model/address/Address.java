package com.restservice.model.address;

import com.restservice.model.person.Person;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@XmlRootElement
@Entity
@NamedQuery(name = "Address.findAddressByAddressLine", query = "select a from Address as a where a.addressLine = :addressLine")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"state", "city", "zipCode", "addressLine"}))
public class Address {
    private int id;
    private String state;
    private String city;
    private String zipCode;
    private String addressLine;


    @JsonIgnore
    private Set<Person> persons;

    public Address() {
    }

    public Address(String state, String city, String zipCode, String addressLine) {
        this.state = state;
        this.city = city;
        this.zipCode = zipCode;
        this.addressLine = addressLine;
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    @ManyToMany(mappedBy = "addresses", fetch = FetchType.LAZY)
    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (id != address.id) return false;
        if (addressLine != null ? !addressLine.equals(address.addressLine) : address.addressLine != null) return false;
        if (city != null ? !city.equals(address.city) : address.city != null) return false;
        if (state != null ? !state.equals(address.state) : address.state != null) return false;
        if (zipCode != null ? !zipCode.equals(address.zipCode) : address.zipCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", addressLine='" + addressLine + '\'' +
                '}';
    }
}
