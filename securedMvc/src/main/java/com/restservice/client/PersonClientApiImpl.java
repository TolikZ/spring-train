package com.restservice.client;

import com.restservice.model.address.Address;
import com.restservice.model.person.Person;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

@Service
public class PersonClientApiImpl implements PersonClientApi {
    private WebResource service;

    public PersonClientApiImpl(){
        ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
//        config.getClasses().add(JacksonJsonProvider.class);
        Client client = Client.create(config);
        service = client.resource(getBaseURI());
    }

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8080").build();
    }

    @Override
    public List<Person> getPersons(){
        ClientResponse response = service.path("restService").path("employee")
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        GenericType<List<Person>> genericType = new GenericType<List<Person>>() {
        };
        return response.getEntity(genericType);
    }

    @Override
    public List<Person> getPersons(int page, int personPerPage){
        ClientResponse response = service.path("restService").path("employee")
                .path("page").path(Integer.toString(page))
                .path("personPerPage").path(Integer.toString(personPerPage))
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        GenericType<List<Person>> genericType = new GenericType<List<Person>>() {
        };
        return response.getEntity(genericType);
    }

    @Override
    public int getPageCount(int personPerPage){
        ClientResponse response = service.path("restService").path("employee")
                .path("countOfPages")
                .path("personPerPage").path(Integer.toString(personPerPage))
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        return response.getEntity(Integer.class);

    }

    @Override
    public Person getPerson(int id){
        ClientResponse response = service.path("restService").path("employee").path(Integer.toString(id))
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != 200){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        return response.getEntity(Person.class);
    }

    @Override
    public Person editPerson(Person person){
        ClientResponse response = service.path("restService").path("employee").type("application/json")
                .accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, person/*"{\"id\":3,\"name\":\"Petrovich\",\"position\":\"Driver\",\"salary\":300}"*/);
        if (response.getStatus() / 100 != 2){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        return response.getEntity(Person.class);
    }

    @Override
    public void deletePerson(int id) {
        ClientResponse response = service.path("restService").path("employee").path(Integer.toString(id))
                .accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        if (response.getStatus() / 100 != 2){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
    }

    @Override
    public Person addPerson(Person person) {
        ClientResponse response = service.path("restService").path("employee").type("application/json")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class, person);
        if (response.getStatus() / 100 != 2){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        return response.getEntity(Person.class);
    }

    @Override
     public Person addAddress(int personId, Address address) {
        ClientResponse response = service.path("restService").path("employee").path(Integer.toString(personId)).type("application/json")
                .accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, address);
        if (response.getStatus() / 100 != 2){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        return response.getEntity(Person.class);
    }

    @Override
    public Person removeAddress(int personId, int addressId) {
        ClientResponse response = service.path("restService").path("employee").path(Integer.toString(personId))
                .path("address").path(Integer.toString(addressId))
                .accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        if (response.getStatus() / 100 != 2){
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }
        return response.getEntity(Person.class);
    }
}
