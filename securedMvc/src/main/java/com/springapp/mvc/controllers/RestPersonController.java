package com.springapp.mvc.controllers;

import com.restservice.client.PersonClientApi;
import com.restservice.model.address.Address;
import com.restservice.model.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/rest")
public class RestPersonController {
    @Autowired
    private PersonClientApi personClientApi;

    private static final int PERSON_PER_PAGE = 10;

    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String showPersons(ModelMap model) {
        model.addAttribute("persons", personClientApi.getPersons(1, PERSON_PER_PAGE));
        model.addAttribute("pageCount", personClientApi.getPageCount(PERSON_PER_PAGE));
        return "rest/showPersons";
    }

    @RequestMapping(value = "/page/{page}", method = RequestMethod.GET)
    public String showPersons(@PathVariable(value = "page") int page, ModelMap model) {
        model.addAttribute("persons", personClientApi.getPersons(page, PERSON_PER_PAGE));
        model.addAttribute("pageCount", personClientApi.getPageCount(PERSON_PER_PAGE));
        return "rest/showPersons";
    }

    @RequestMapping(value = "/{id}")
    public String showPerson(@PathVariable(value = "id") int id, ModelMap model){
        model.addAttribute("person", personClientApi.getPerson(id));
        return "rest/showPerson";
    }

    @RequestMapping(value = "/remove/{id}")
    public String deletePerson(@PathVariable(value = "id") int id, ModelMap model){
        personClientApi.deletePerson(id);
        return "redirect:/rest/";
    }

    @RequestMapping(value = "/edit/{id}")
    public String editPerson(@PathVariable(value = "id") int personId, ModelMap model){
        model.addAttribute("editedPerson", personClientApi.getPerson(personId));
        model.addAttribute("addedAddress", new Address());
        return "/rest/editPerson";
    }

    @RequestMapping(value = "/editPerson", params = {"id"})
    public String editPerson(@ModelAttribute Person person, ModelMap model){
        personClientApi.editPerson(person);
        return "redirect:/rest/" + Integer.toString(person.getId());
    }

    @RequestMapping(value = "/add")
    public String addPerson(ModelMap model){
        model.addAttribute("newPerson", new Person());
        return "/rest/addPerson";
    }

    @RequestMapping(value = "/add", params = {"id"})
    public String addPerson(@ModelAttribute Person person, ModelMap model){
        Person resPerson = personClientApi.addPerson(person);
        return "redirect:/rest/" + resPerson.getId();
    }

    @RequestMapping(value = "/edit/{id}/removeAddress/{addressId}")
    public String removePersonAddress(@PathVariable(value = "id") int personId, @PathVariable(value = "addressId") int addressId, ModelMap model){
        personClientApi.removeAddress(personId, addressId);
        model.addAttribute("editedPerson", personClientApi.getPerson(personId));
        return "redirect:/rest/edit/" + personId;
    }

    @RequestMapping(value = "/edit/{personId}/addAddress", params = {"id"})
    public String addAddress(@PathVariable(value = "personId") int personId, @ModelAttribute Address address, ModelMap model){
        Person person = personClientApi.getPerson(personId);
        person.addAddress(address);
        personClientApi.editPerson(person);
        return "redirect:/rest/edit/" + person.getId();
    }
}
