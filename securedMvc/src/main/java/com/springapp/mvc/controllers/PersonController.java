package com.springapp.mvc.controllers;

import com.springapp.mvc.model.Person;
import com.springapp.mvc.model.PersonDao;
import com.springapp.mvc.model.PersonImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;

//@Controller
//@RequestMapping("/")
public class PersonController {
    @Autowired
    PersonDao personDao;

    @Autowired
    private PropertyEditorSupport personBinder;

    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String showPersons(ModelMap model) {
        model.addAttribute("persons", personDao.getPersons());
        return "showPersons";
    }

    @RequestMapping(value = "/admin/addPerson")
    public String addPersonDialog(ModelMap model){
        model.addAttribute("newPerson", new PersonImpl());

//        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        model.addAttribute("userName", user.getUsername());
//        model.addAttribute("userPassword", user.getPassword());

        return "addPerson";
    }

    @RequestMapping(value = "/admin/addPerson", params = {"id"})
    public String addPerson(@ModelAttribute @Valid PersonImpl person, BindingResult bindingResult, ModelMap model){
        if (bindingResult.hasErrors()){
            model.addAttribute("validationErrorCount", bindingResult.getFieldErrorCount());
            model.addAttribute("newPerson", person);
            return "addPerson";
        };

        personDao.addPerson(person);
        return "redirect:/showPersons";
    }

    @RequestMapping(value = "/secure/editPerson/{id}")
    public String editPerson(@PathVariable(value = "id") Person person, ModelMap model){
        model.addAttribute("editedPerson", person);
        return "editPerson";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Person.class, personBinder);
    }
}
