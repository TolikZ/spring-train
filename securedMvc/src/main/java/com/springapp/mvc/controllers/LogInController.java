package com.springapp.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LogInController {
    @RequestMapping(value = "/loginPage")
    public String login(ModelMap model){
        return "login";
    }

    @RequestMapping(value = "/loginFailure")
    public String loginFailure(ModelMap model){
        return "loginFailure";
    }
}
