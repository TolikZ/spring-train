package com.springapp.mvc.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.PropertyEditorSupport;

@Service
public class PersonBinder extends PropertyEditorSupport{
    private PersonDao persons;

    @Autowired
    public void setPersons(PersonDao persons){
        this.persons = persons;
    }

    @Override
    public void setAsText(String id) throws IllegalArgumentException {
        Person person = persons.getPerson(Integer.parseInt(id));
        if (person == null){
            person = new PersonImpl();
            person.setId(Integer.parseInt(id));
            setValue(person);

        } else{
            setValue(persons.getPerson(Integer.parseInt(id)));
        }
    }


}
