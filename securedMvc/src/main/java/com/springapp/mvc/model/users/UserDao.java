package com.springapp.mvc.model.users;

public interface UserDao {
    public User getUser(String name);
}
