package com.springapp.mvc.model;

import java.util.List;

public interface PersonDao {
    public List<Person> getPersons();
    public Person getPerson(int id);
    public void addPerson(Person person);
}
