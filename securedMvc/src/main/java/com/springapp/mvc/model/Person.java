package com.springapp.mvc.model;

public interface Person {
    public int getId();
    public String getName();
    public String getPosition();
    public int getSalary();

    public void setId(int id);
    public void setName(String name);
    public void setPosition(String position);
    public void setSalary(int salary);
}
