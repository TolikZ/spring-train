package com.springapp.mvc.model.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MyConstraintValidator implements ConstraintValidator<Position, String> {
    @Override
    public void initialize(Position position) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null){
            return false;
        }
        return s.matches("[\\d\\w]+");
    }
}
