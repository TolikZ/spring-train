package com.springapp.mvc.model;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class HashMapPersonDao implements PersonDao {
    private Map<Integer, Person> persons = new ConcurrentHashMap<Integer, Person>();
    @Override
    public List<Person> getPersons() {
        return new ArrayList(persons.values());
    }

    @Override
    public Person getPerson(int id) {
        return persons.get(id);
    }

    @Override
    public void addPerson(Person person) {
        persons.put(person.getId(), person);
    }

    @PostConstruct
    public void init(){
        persons.put(1, new PersonImpl(1, "Vasya", "Cleaner", 500));
        persons.put(2, new PersonImpl(2, "Valerii Georgievich", "Director", 150));
        persons.put(3, new PersonImpl(3, "Petrovich", "Driver", 300));
    }
}
