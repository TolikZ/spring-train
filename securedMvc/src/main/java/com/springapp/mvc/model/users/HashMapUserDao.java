package com.springapp.mvc.model.users;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class HashMapUserDao implements UserDao {
    private Map<String, User> users = new ConcurrentHashMap<String, User>();

    @Override
    public User getUser(String name) {

        return users.get(name);
    }

    @PostConstruct
    public void init(){

        users.put("Bot1", new User("Bot1", "123", "ROLE_USER,ROLE_ADMIN"));
        users.put("Bot2", new User("Bot1", "123", "ROLE_USER"));
    }
}
